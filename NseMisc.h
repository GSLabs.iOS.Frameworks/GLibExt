//
//  NseMisc.h
//  GLibExt
//
//  Created by Dan on 26.10.2021.
//

#import "NseMain.h"

#define NSE_BOX(value) \
    ({ \
        (value != NULL) ? @(value) : nil; \
    })
