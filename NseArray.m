//
//  NseArray.m
//  GLibExt
//
//  Created by Dan Kalinin on 1/26/20.
//

#import "NseArray.h"

@implementation NSArray (GArray)

- (GArray *)gArrayGintTo {
    GArray *ret = g_array_new(FALSE, TRUE, sizeof(gint));
    
    for (NSNumber *value in self) {
        gint sdkValue = value.intValue;
        (void)g_array_append_val(ret, sdkValue);
    }
    
    return ret;
}

@end

@implementation NSArray (GPtrArray)

- (GPtrArray *)gPtrArrayGchararrayTo {
    GPtrArray *ret = g_ptr_array_new_with_free_func(g_free);
    
    for (NSString *string in self) {
        g_ptr_array_add(ret, g_strdup(string.UTF8String));
    }
    
    return ret;
}

@end

@implementation NSArray (GList)

+ (instancetype)gListGintFrom:(GList *)sdkValues {
    NSMutableArray<NSNumber *> *ret = NSMutableArray.array;

    for (GList *sdkValue = sdkValues; sdkValue != NULL; sdkValue = sdkValue->next) {
        [ret addObject:@(GPOINTER_TO_INT(sdkValue->data))];
    }

    return ret;
}

+ (instancetype)gListGchararrayFrom:(GList *)sdkValues {
    NSMutableArray<NSString *> *ret = NSMutableArray.array;
    
    for (GList *sdkValue = sdkValues; sdkValue != NULL; sdkValue = sdkValue->next) {
        [ret addObject:NSE_BOX((gchar *)sdkValue->data)];
    }
    
    return ret;
}

- (GList *)gListGintTo {
    GList *ret = NULL;
    
    for (NSNumber *number in self) {
        ret = g_list_append(ret, GINT_TO_POINTER(number.intValue));
    }
    
    return ret;
}

- (GList *)gListGchararrayTo {
    GList *ret = NULL;
    
    for (NSString *string in self) {
        ret = g_list_append(ret, g_strdup(string.UTF8String));
    }
    
    return ret;
}

@end

@implementation NSArray (GStrv)

- (GStrv)gStrvTo {
    GStrv ret = g_new0(gchar *, self.count + 1);
    
    for (NSUInteger i = 0; i < self.count; i++) {
        ret[i] = g_strdup(((NSString *)self[i]).UTF8String);
    }
    
    return ret;
}

@end

@implementation NSArray (GHashTable)

- (GHashTable *)gHashTableGchararrayGchararrayTo {
    GHashTable *ret = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
    
    for (NSUInteger i = 0; i < self.count; i += 2) {
        gchar *key = g_strdup(((NSString *)self[i]).UTF8String);
        gchar *value = g_strdup(((NSString *)self[i + 1]).UTF8String);
        (void)g_hash_table_insert(ret, key, value);
    }
    
    return ret;
}

@end

@implementation NseArray

- (instancetype)initWithStorage:(NSPointerArray *)storage {
    self = super.init;
    
    if (self != nil) {
        _storage = storage;
    }
    
    return self;
}

- (void)forwardInvocation:(NSInvocation *)anInvocation {
    [_storage compact];
    
    for (id anObject in self) {
        if ([anObject respondsToSelector:anInvocation.selector]) {
            [anInvocation invokeWithTarget:anObject];
        }
    }
}

- (NSUInteger)count {
    return _storage.count;
}

- (id)objectAtIndex:(NSUInteger)index {
    void *ret = [_storage pointerAtIndex:index];
    return (__bridge id)ret;
}

- (void)insertObject:(id)anObject atIndex:(NSUInteger)index {
    [_storage insertPointer:(__bridge void *)anObject atIndex:index];
}

- (void)removeObjectAtIndex:(NSUInteger)index {
    [_storage removePointerAtIndex:index];
}

- (void)addObject:(id)anObject {
    [_storage addPointer:(__bridge void *)anObject];
}

- (void)removeLastObject {
    NSUInteger index = _storage.count - 1;
    [_storage removePointerAtIndex:index];
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject {
    [_storage replacePointerAtIndex:index withPointer:(__bridge void *)anObject];
}

@end
