//
//  NseCancellable.m
//  GLibExt
//
//  Created by Dan on 10.07.2021.
//

#import "NseCancellable.h"

@implementation NseCancellable

+ (instancetype)cancellable {
    NseCancellable *ret = self.new;
    ret.object = g_cancellable_new();
    return ret;
}

- (void)dealloc {
    g_object_unref(self.object);
}

- (void)cancel {
    g_cancellable_cancel(self.object);
}

@end
