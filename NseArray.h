//
//  NseArray.h
//  GLibExt
//
//  Created by Dan Kalinin on 1/26/20.
//

#import "NseMain.h"
#import "NseMisc.h"

@interface NSArray (GArray)

- (GArray *)gArrayGintTo;

@end

@interface NSArray (GPtrArray)

- (GPtrArray *)gPtrArrayGchararrayTo;

@end

@interface NSArray (GList)

+ (instancetype)gListGintFrom:(GList *)sdkValues;
+ (instancetype)gListGchararrayFrom:(GList *)sdkValues;

- (GList *)gListGintTo;
- (GList *)gListGchararrayTo;

@end

@interface NSArray (GStrv)

- (GStrv)gStrvTo;

@end

@interface NSArray (GHashTable)

- (GHashTable *)gHashTableGchararrayGchararrayTo;

@end

@interface NseArray : NSMutableArray

@property (readonly) NSPointerArray *storage;

- (instancetype)initWithStorage:(NSPointerArray *)storage;

@end
