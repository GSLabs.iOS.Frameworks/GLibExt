//
//  NseError.h
//  GLibExt
//
//  Created by Dan Kalinin on 12/12/19.
//

#import "NseMain.h"
#import "NseMisc.h"

@interface NSError (GError)

+ (instancetype)gErrorFrom:(GError *)sdkError;

@end

@interface NseError : NSError

@end
