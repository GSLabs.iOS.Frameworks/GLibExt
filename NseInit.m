//
//  NseInit.m
//  GLibExt
//
//  Created by Dan on 26.10.2021.
//

#import "NseInit.h"

@implementation NseInit

+ (void)initialize {
    ge_init();
    ge_init_l10n((gchar *)NSLocale.currentLocale.languageCode.UTF8String);
    ge_init_tz((gint)NSTimeZone.systemTimeZone.secondsFromGMT);
    ge_init_schemas((gchar *)NSFileManager.defaultManager.temporaryDirectory.path.UTF8String);
}

@end
