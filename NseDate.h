//
//  NseDate.h
//  GLibExt
//
//  Created by Dan on 03.07.2021.
//

#import "NseMain.h"

@interface NSDate (GDateTime)

+ (instancetype)gDateTimeFrom:(GDateTime *)sdkDateTime;

@end
